<?php

abstract class Pc {

    abstract function component();
}

abstract class Monitor {

    abstract function risoluzione();
}

abstract class Stampante {

    abstract function tipo() ;


}

class MatherBoard extends Pc {

    function component() {
    echo "Sono la Scheda Madre del PC \n";


    }
}

class  CardVideo extends PC {

     function component()  {
     echo "Sono la Scheda Video \n" ;
        

     }
}


class SoundCard extends PC {

 function component() {

    echo "Sono la scheda Audio \n";

}

}

class Samsung extends Monitor {

    function risoluzione() {
       echo "Sono il monitor Samsung ed ho una riosoluzione di 1200 \n" ;


    }
}

class Lg extends Monitor {

    function risoluzione() {
     echo "Sono il monitor Lg ed ho una risoluzione di 1900 \n" ;

    }

}


class Aghi extends Stampante {
 
   function tipo() {

    echo "Sono una stampante ad Aghi \n";

   } 

}


class Laser extends Stampante {
 
    function tipo() {
 
     echo "Sono una stampante ad Laser \n";
 
    } 
 
 }

 class Computer { 
    
    public $velocità;
    public $schermo;
    public $periferiche;

    public   function __construct( Pc $pc, Monitor $monitor, Stampante $stampante ) {

    $this->velocità =$pc;
    $this->schermo=$monitor;    
    $this->periferiche=$stampante;

    }
    
    public function processore() {
    $this->velocità->component();    

    }

    public function componenti() {
    $this->schermo->risoluzione();

    }
    public function stampanti() {
    $this->periferiche->tipo();

    }
 }

 $Computer1 = new Computer( new MatherBoard(), new Samsung (), new Aghi ());

 $Computer1->processore() ;
 $Computer1->componenti() ;
 $Computer1->stampanti() ;
 

